const apiKey = 'CpvvxFt-E0D98-hm2d8hFA'

// Add a subscriber to the ConvertKit form
async function subscribe(firstName, email) {
  let response = await fetch('https://api.convertkit.com/v3/forms/2958911/subscribe', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify({
      first_name: firstName,
      email: email,
      api_key: apiKey
    })
  })
  if (response.ok) {
    alert("Success! Now check your email to confirm your subscription.")
  } else {
    alert("Something went wrong. Double-check your name and email and try again. Reload the page if this continues.")
  }
  console.log(response)
}

// When the user clicks "Sign up", subscribe them!
signupForm = document.querySelector('.signup_form')
signupForm.addEventListener('submit', (event) => {
  let firstName = document.getElementById('text_first-name').value
  let email = document.getElementById('email_address').value
  subscribe(firstName, email)
  event.preventDefault()
})
